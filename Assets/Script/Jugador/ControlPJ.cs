﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlPJ : MonoBehaviour {

    public delegate void BotonSalto(Vector2 dir);
    public event BotonSalto salto;


    public delegate void BotonSaltoPared(bool der);
    public event BotonSaltoPared saltoPared;

    public event BotonSaltoPared aterrizajePared;



    public delegate void GameOverDelegate();
    public event GameOverDelegate gameOverEvent;


    public Vector2 fSalto = new Vector2(5, 5);
    public Vector2 fSaltoAereo = new Vector2(3, 6); 
    private Collider2D col;
    private Rigidbody2D rb;
    private bool enParedIzq = false;
    private bool enParedDer = false;

    public LayerMask collisionMask;

    [Header("WallCheck")]
    private float horizontalRaySpacing;
    public int horizontalRayCount;
    public float xRayLength;
    public Vector2 offsetRayos;
    /// <summary>
    /// True, permite que se reinicie el contador de salto aereo al tocar una pared
    /// </summary>
    public bool saltoAereoActivado;

    [Header("Comportamiento en pared")]
    private bool enSalto = false;        //Si se esta realizando un salto
    private bool enAterrizaje = false;
    public float velYnormal = 1;         //velocidad a la que reestaura la alturaY "normal"
    public float alturaNormal = -3;      //AlturaY normal

    bool gameOver = false;
    public bool saltoAereo = true;

    public void Awake()
    {
        col = GetComponent<Collider2D>();
        rb = GetComponent<Rigidbody2D>();
    }
    
    public float GetDistanciaAlBordeX()
    {
        return col.bounds.size.x;
    }

    public float GetDistanciaAlBordeY()
    {
        return col.bounds.size.y;
    }

    /// <summary>
    /// der FALSE = lado izquiero || TRUE = lado derecho
    /// EnParedIzq y EnParedDer chequean si hay contacto en alguno de los lados del objeto usando Raycast.
    /// horizontalRaySpacing se calcula dividiendo la mitad inferior de la altura por la cantidad de rayos a desplegar
    /// horizontalRayCount la cantidad de rayos a desplegar
    /// </summary>
    /// <returns></returns>
    public bool CheckPared(bool Der)
    {                                               

        horizontalRaySpacing = (col.bounds.size.y / 2) / (horizontalRayCount - 1);                                                    //Calcular distancia entre rayos
        for (int i = 0; i < horizontalRayCount; i++)
        {
            int directionX;
            Vector2 rayOrigin;
            if (Der)
            {
                rayOrigin = new Vector2(col.bounds.max.x, col.bounds.min.y) + Vector2.right * offsetRayos.x;     //Ubicar primera punta en lado derecho
                directionX = 1;
            }
            else
            {
                rayOrigin = new Vector2(col.bounds.min.x, col.bounds.min.y) - Vector2.right * offsetRayos.x;     //IDEM en lado derecho
                directionX = -1;
            }
            rayOrigin += Vector2.up * (horizontalRaySpacing * i);                                                                       //Sumar cantidades de distancia de acuerdo al numero del rayo
            RaycastHit2D hit = Physics2D.Raycast(rayOrigin, Vector2.right * directionX, xRayLength, collisionMask);
            Debug.DrawRay(rayOrigin, Vector2.right * directionX * xRayLength, Color.red);
            if (hit)
            {
                return hit;
            }
        }
        return false;
    }

    public void FixedUpdate()
    {
        if (gameOver) return;
        enParedIzq = CheckPared(false);
        enParedDer = CheckPared(true);
        if ((enParedDer || enParedIzq) && saltoAereoActivado) saltoAereo = false;
        PegarseEnPared();
    }

    public void Salto(bool der)
    {
        if ((der && enParedIzq) || (!der && enParedDer))
        {
            enSalto = true;
            rb.velocity = Vector2.zero;
            if (der)
            {
                rb.AddForce(fSalto, ForceMode2D.Impulse);                
                if(saltoPared != null) saltoPared(true);
            }
            else
            {
                rb.AddForce(new Vector2(-fSalto.x, fSalto.y), ForceMode2D.Impulse);
                if (saltoPared != null) saltoPared(false);
            }
        }
        else if (!saltoAereo && !enParedDer && !enParedIzq) SaltoAereo(der);        
    }

    public void EventoSalto(Vector3 dir)
    {
        if (salto != null)
        {
            salto(dir.normalized);
        }
    }

    public void SaltoAereo (bool der)
    {
        rb.velocity = Vector2.zero;
        if (der)
        {
            rb.AddForce(fSaltoAereo, ForceMode2D.Impulse);
            EventoSalto(fSaltoAereo);
        }
        else
        {
            rb.AddForce(new Vector2(-fSaltoAereo.x, fSaltoAereo.y), ForceMode2D.Impulse);
            EventoSalto(new Vector2(-fSaltoAereo.x, fSaltoAereo.y));
        }
        saltoAereo = true;
    }
        
    void PegarseEnPared()
    {
        //print(enSalto);
        if (enAterrizaje && enParedDer)
        {
            enAterrizaje = false;
            if (aterrizajePared != null)
            {
                aterrizajePared(false);
            }
        }
        else if (enAterrizaje && enParedIzq)
        {
            if (aterrizajePared != null)
            {
                aterrizajePared(true);
            }
            enAterrizaje = false;
        }

        if ((enParedDer || enParedIzq) && !enSalto)
        {
            rb.velocity = Vector2.zero;
            transform.position = Vector2.Lerp(transform.position, new Vector2(transform.position.x, alturaNormal), velYnormal * Time.deltaTime);
            //print("pegado");
        }
        else if (!enParedDer && !enParedIzq)
        {
            enSalto = false;
            enAterrizaje = true;
        }
    }

    public void GameOver()
    {
        if (gameOver) return;
        else if(gameOverEvent != null)
        {
            gameOverEvent();            
        }
        rb.gravityScale *= 1.5f;
        gameOver = true;
    }
}
