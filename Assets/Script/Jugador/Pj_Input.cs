﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pj_Input : MonoBehaviour {

    public ControlPJ control;
    bool gameOver = false;

	void Start ()
    {
        control.gameOverEvent += GameOver;
	}
	
	void GameOver()
    {
        gameOver = true;
    }

	void Update ()
    {
        if(gameOver)
        {
            return;
        }
        if (Input.GetKeyDown(KeyCode.A)) control.Salto(false);
        if (Input.GetKeyDown(KeyCode.D)) control.Salto(true);
	}
}
