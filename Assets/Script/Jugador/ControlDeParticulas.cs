﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ControlPJ))]
public class ControlDeParticulas : MonoBehaviour {

    public GameObject particula;
    public float intensidad = 5;
    public float distanciaX;
    public float distanciaY;

    public int cantidad = 5;
    public float variacionDirMax = 0.2f;
    public float variacionIntensidad = 0.2f;
    public float variacionPos = 0.5f;
    public float variacionPosYenPared = 0.3f;


    private float distanciaAlBorde;

    

    ControlPJ cPJ;

    void Start()
    {
        cPJ = GetComponent<ControlPJ>();
        distanciaX = cPJ.GetDistanciaAlBordeX();
        distanciaY = cPJ.GetDistanciaAlBordeY();

        cPJ.salto += DispararRafaga;
        cPJ.saltoPared += EfectoSaltoEnPared;
        //cPJ.aterrizajePared += EfectoAterrizaje;
    }

    public void DispararRafaga(Vector2 dirOrig)
    {
        for (int i = 0; i < cantidad; i++)
        {
            GameObject copia = (GameObject)Instantiate<GameObject>(particula , VariarPos( transform.position - ((Vector3)dirOrig * distanciaAlBorde)), transform.rotation);
            copia.GetComponent<MovParticula>().SetMovimiento(-VariarDireccion(dirOrig), VariarIntensidad(intensidad), distanciaX);
            Destroy(copia.gameObject, 2f);
        }
    }

    public void EfectoSaltoEnPared(bool Der)
    {
        float mod = 1;
        if (Der) mod = -1;


        for (int i = 0; i < cantidad; i++)
        {
            GameObject copia = (GameObject)Instantiate<GameObject>(particula, VariarPosEnPared(transform.position + new Vector3(distanciaX * mod/2, -distanciaY/4)), transform.rotation);
            copia.GetComponent<MovParticula>().SetMovimiento(VariarDirEnPared((int)mod), VariarIntensidad(intensidad), distanciaX);
            Destroy(copia.gameObject, 2f);
        }
    }

    public void EfectoAterrizaje(bool Der)
    {
        float mod = 1;
        if (Der) mod = -1;

        for (int i = 0; i < cantidad/3; i++)
        {
            GameObject copia = (GameObject)Instantiate<GameObject>(particula, VariarPosEnPared(transform.position + new Vector3(distanciaX * mod / 2, distanciaY / 4)), transform.rotation);
            copia.GetComponent<MovParticula>().SetMovimiento(VariarDirEnPared((int)mod), VariarIntensidad(intensidad), distanciaX);
            Destroy(copia.gameObject, 2f);
        }
    }

    Vector3 VariarPosEnPared(Vector3 pos)
    {
        float y = Random.Range(-variacionPosYenPared, variacionPosYenPared);
        return pos + new Vector3(0, y);

    }

    Vector2 VariarDirEnPared(int mod)
    {
        Vector2 dir = new Vector2();

        dir.x = -mod;
        dir.y = Random.Range(-3, 3);
        dir.Normalize();
        return dir;
    }



    Vector2 VariarDireccion(Vector3 dir)
    {
        dir.x = dir.x + Random.Range(-variacionDirMax, variacionDirMax);
        dir.y = dir.y + Random.Range(-variacionDirMax, variacionDirMax);
        dir.Normalize();
        return dir;
    }


    Vector2 VariarPos(Vector3 pos)
    {
        pos.x = pos.x + Random.Range(-variacionPos, variacionPos);
        pos.y = pos.y + Random.Range(-variacionPos, variacionPos);
        return pos;
    }

    float VariarIntensidad(float intensidad)
    {
        return Random.Range(intensidad - variacionIntensidad, intensidad + variacionIntensidad);
    }

}
