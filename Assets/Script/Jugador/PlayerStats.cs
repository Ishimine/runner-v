﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats
{
    public int saltos;
    public int puntaje;
    public int monedas;
    public int powerUps;
    public float tiempo;
}
