﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void CargarNivel(int x)
    {
        SceneManager.LoadScene(x);
    }
    public void Salir()
    {
        Application.Quit();
    }
}
