﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirectorDeEscenario : MonoBehaviour {

    public delegate void Evento ();
    public event Evento cambioDif;

    public delegate void EventoSimple ();
    public event EventoSimple actualizacion;


    public SpawnEscenarios spawnEsc;
    public Spawn_ControlGeneral spawnCG;
    /// <summary>
    /// Si se activa, la duracion de los escenarios sera igual a la DuracionGlobal dividido la cantidad de escenarios en total. Como consecuencia todos los escenarios
    /// tendran la misma duracion.
    /// </summary>
    public bool usarDuracionGlobal = true;
    public float duracionGlobal;

    public float tiempoEscenarioActual = 2f;
    public float tiempoActual;
    
    public int iEscActual = -1;    
    public Escenario[] escenarios;

    private bool enUltEsc;

    void Awake()
    {
        iEscActual = -1;
        SiguienteEscenario();
        spawnEsc.BuscarPiezaSiguiente();
        spawnEsc.go = true;
    }

    void Update()
    {
        if(enUltEsc)
        {
            return;
        }
        tiempoActual += Time.deltaTime;
        if(tiempoActual >= tiempoEscenarioActual)
        {
            tiempoActual = 0;
            SiguienteEscenario();            
        }
    }

    void SiguienteEscenario()
    {
  
        iEscActual++;
        if (usarDuracionGlobal) tiempoEscenarioActual = duracionGlobal;
        else tiempoEscenarioActual = escenarios[iEscActual].duracion;
        spawnEsc.CargarNuevoEscenario(escenarios[iEscActual]);
        spawnCG.velActual = -escenarios[iEscActual].Velocidad;

        Camera.main.GetComponent<Camera>().backgroundColor = escenarios[iEscActual].colorFondo;

        if (iEscActual == escenarios.Length-1)
        {
            enUltEsc = true;
        }
        if (cambioDif != null)
        {
            cambioDif();
        }
    }

    void CalcularPorcentajes()
    {
        for (int i = 0; i < escenarios.Length; i++)
        {
            //Calculamos el total
            float total = 0;
            for (int a = 0; a < escenarios[i].piezas.Length; a++)
            {
                total += escenarios[i].piezas[a].valor;
            }
            //Repartimos las probabilidades en base del total
            for (int a = 0; a < escenarios[i].piezas.Length; a++)
            {
                escenarios[i].piezas[a].SetProbabilidad((100 / (total / escenarios[i].piezas[a].valor)));
            }
        }
    }

    void OnValidate()
    {
        CalcularPorcentajes();
        ActualizarNombresPiezas();
        if (actualizacion != null) actualizacion();
    }

    void ActualizarNombresPiezas()
    {
        for(int i = 0; i< escenarios.Length;i++)
        {
            for(int a = 0; a < escenarios[i].piezas.Length; a++)
            {
                escenarios[i].piezas[a].nombre = escenarios[i].piezas[a].pieza.name;
            }
        }
    }
}
