﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public GameObject gameOverScreen;
    public Text puntajeUI;
    public Text puntajeUIfinal;
    public GameObject recordUI;
    public Text recordTxt;
    public Text dificultadUI;
    public GameObject nuevoRecordUI;

    public float puntaje = 0;
    public float multDePuntaje = 125;
    public float valorMoneda = 500;
    bool gameOver = false;



    void Start ()
    {
        FindObjectOfType<ControlPJ>().gameOverEvent += GameOver;	
	}

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!gameOver)
        {
            ActualizarPuntaje();
        }
    }

    public void ActualizarPuntaje()
    {
        puntaje += multDePuntaje * Time.fixedDeltaTime;
        puntajeUI.text = puntaje.ToString("0");
    }

    public void SumarMoneda()
    {
        puntaje += valorMoneda;
    }


    void GameOver()
    {
        puntajeUI.gameObject.SetActive(false);
        puntajeUIfinal.text = puntaje.ToString("0");
        gameOverScreen.SetActive(true);
        if(CompararConPutajeMaximo((int)puntaje))
        {
            GuardarNuevoPuntajeMaximo((int)puntaje);
            nuevoRecordUI.SetActive(true);
            recordUI.gameObject.SetActive(false);
        }
        else
        {
            recordTxt.text = PlayerPrefs.GetInt("Puntaje Maximo").ToString();
        }
    }

    public void RestartLevel()
    {
        SceneManager.LoadScene("Test_00");
    }

    

    bool CompararConPutajeMaximo(int newPuntaje)
    {
        if(newPuntaje > PlayerPrefs.GetInt("Puntaje Maximo"))
        {
            return true;
        }
        return false;
    }
    void GuardarNuevoPuntajeMaximo(int nuevoPuntajeMaximo)
    {
        PlayerPrefs.SetInt("Puntaje Maximo", nuevoPuntajeMaximo);
    }
}
