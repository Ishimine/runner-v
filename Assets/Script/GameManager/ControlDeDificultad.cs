﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlDeDificultad : MonoBehaviour {

    public enum ModoDificultad {Gradual, Manual, PorNiveles}
    public ModoDificultad AumentoDeDificultad;
    public bool mostrarDistanciaEntrePiezas;

    public Spawn_ControlGeneral SpawnCG;
    public SpawnDinamico SpawnD;
    public GameManager gm;

    [Header("Dificultad Manual")]
    public NivelDeDificultad[] NivelesDeDificultad;

    int nivelActual = 0;


    [SerializeField] float contador;
    [SerializeField] float tiempoPorNivel;
    bool enUltNivel = false;
    public bool actualizarNombre;

    [Header("Dificultad Gradual")]
    public float velInicial = -4;
    public float velActual;
    public float velMax = -6;
    /// <summary>
    /// Segundos que se tarde de ir de la dificultad inicial a la Maxima
    /// </summary>
    public float tiempoTransicion;

    [Header("Dificultad Por Escenarios")]
    public Escenario[] Escenarios;


    void Awake ()
    {
        if(gm == null)
        {
            gm = FindObjectOfType<GameManager>();
        }
        if(SpawnCG == null)
        {
            SpawnCG = FindObjectOfType<Spawn_ControlGeneral>();
        }
        if (SpawnD == null)
        {
            SpawnD = FindObjectOfType<SpawnDinamico>();
        }

        if (AumentoDeDificultad == ModoDificultad.Gradual)
        {
            SpawnCG.gradual = true;
        }
        else
        {
            SpawnCG.gradual = false;
        }

        if(AumentoDeDificultad == ModoDificultad.Manual)
        {
            contador = 0;
            tiempoPorNivel = CalcularTiempoPorNivel();
            ActivarNivel(0);
        }
        
        if(mostrarDistanciaEntrePiezas) SpawnD.mostrarTimingDePieza = true;
        else SpawnD.mostrarTimingDePieza = false;

    }
	
    float CalcularTiempoPorNivel()
    {
        return tiempoTransicion / NivelesDeDificultad.Length;
    }

    void ActivarNivel(int i)
    {
        SpawnCG.velActual = NivelesDeDificultad[i].Velocidad;
        velActual = NivelesDeDificultad[i].Velocidad;
        SpawnD.ReemplazarProbPorTipo(NivelesDeDificultad[i].pickUp, NivelesDeDificultad[i].sueltas,NivelesDeDificultad[i].bloques, NivelesDeDificultad[i].vacio);

        SpawnD.ReemplazarProbPorDificultad(NivelesDeDificultad[i].facil, NivelesDeDificultad[i].medio, NivelesDeDificultad[i].dificil);

        SpawnD.ReemplazarEspacios(NivelesDeDificultad[i].espacioEntrePiezas, NivelesDeDificultad[i].espacioVacios);

     //   SpawnD.CalcularPorcentajesPorDificultad();
      //  SpawnD.CalcularPorcentajesPorTipo();

        if (actualizarNombre)
        {
            gm.dificultadUI.text = NivelesDeDificultad[i].nombreDelNivel;
        }

    }

	void Update ()
    {
        if (AumentoDeDificultad == ModoDificultad.Manual)
        {
            contador += Time.deltaTime;
            if (!enUltNivel && contador >= tiempoPorNivel)
            {
                contador = 0;
                nivelActual += 1;
                ActivarNivel(nivelActual);
                if(nivelActual == NivelesDeDificultad.Length-1)
                {
                    enUltNivel = true;
                }
            }
        }
    }
}
