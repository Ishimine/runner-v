﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NombreDificultad : MonoBehaviour {

    public DirectorDeEscenario dirEsc;
    public Text txt;

	void Awake ()
    {
        txt = GetComponent<Text>();
       if(dirEsc == null) dirEsc = FindObjectOfType<DirectorDeEscenario>();
       dirEsc.cambioDif += actualizarNombreDificultad;
       
    }
	
	void actualizarNombreDificultad()
    {
        //print("sada");
        txt.text = dirEsc.escenarios[dirEsc.iEscActual].nombreDelEscenario;
    }
}
