﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fps_Contador : MonoBehaviour {

    Text txt;
    float fps;

    void Start ()
    {
        txt = GetComponent<Text>();
        StartCoroutine(RecalcularFPS());
	}


    private IEnumerator RecalcularFPS()
    {
        while(true)
        {
            fps = 1 / Time.deltaTime;
            txt.text = fps.ToString("000");
            yield return new WaitForSeconds(1);
        }
    }


}
