﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Sprite))]
public class MovParticula : MonoBehaviour
{

    public SpriteRenderer sprt;
    Vector2 dir;
    float intensidad;
    float dist;
    Vector2 dirObj;
    Vector2 vel;
    float velAlpha;
    public float t;
    public Vector3 escalaMax = new Vector3(0.15f, 0.15f, 1);
    public float tCrecimiento;
    bool crecimiento = true;
    float cont = 0;

    public float velTerreno = -1;
    public float i = 0;


    public void SetMovimiento(Vector3 dir, float intensidad, float dist)
    {
        this.dir = dir;
        this.dist = dist;
        this.intensidad = intensidad;
        dirObj = transform.position + dir * dist;
    }


    void Update()
    {
        ActualizarCaida();
        ActualizarAlpha();
        transform.position = Vector2.SmoothDamp(transform.position, dirObj, ref vel, 10 / intensidad, 10, Time.deltaTime);
        ActualizacionEscala();
        cont += Time.deltaTime;
    }

    void ActualizarAlpha()
    {
        sprt.color = Color.Lerp(sprt.color, new Vector4(255,255,255,0), 10 / intensidad * Time.deltaTime);
    }

    void ActualizarCaida()
    {
        velTerreno = Spawn_ControlGeneral.velCaidaGlobal;
        transform.position += new Vector3(0, velTerreno * Time.deltaTime, 0);
        dirObj += new Vector2(0, velTerreno * Time.deltaTime);
    }

    void ActualizacionEscala()
    {
        if (crecimiento)
        {
            transform.localScale = Vector3.Lerp(transform.localScale, escalaMax, t * Time.deltaTime);
            if(cont >= tCrecimiento)
            {
                crecimiento = false;
            }
        }
        else
        {
            transform.localScale = Vector3.Lerp(transform.localScale, Vector3.zero, t * Time.deltaTime / 14);
        }
    }

}
