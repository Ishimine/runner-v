﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class textoPopUp : MonoBehaviour {

    public string txt;
    public float vel;

    float yIni;
    public float alturaDeAnim = 0.5f;
    public float tiempoDeVida = 1.5f;
    float velRef;

    void Start()
    {
        yIni = transform.position.y;
        Destroy(gameObject, tiempoDeVida);
    }

    void Update()
    {
        //transform.position = new Vector3(transform.position.x, Mathf.Lerp(yIni, yIni + alturaDeAnim, vel * Time.deltaTime));
        transform.position = new Vector3(transform.position.x, Mathf.SmoothDamp(transform.position.y, yIni + alturaDeAnim,ref velRef, vel * Time.deltaTime));        
    }
}
