﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaEliminacion : MonoBehaviour {



    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            other.gameObject.GetComponent<ControlPJ>().GameOver();
            Destroy(other.gameObject);
        }
        else
        {
            Destroy(other.gameObject);
        }
    }
}
