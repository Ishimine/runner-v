﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstaculoLetal : MonoBehaviour {
       
	
	void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.tag == "Player")
        {
            print("GameOver");
            other.gameObject.GetComponent<ControlPJ>().GameOver();
        }
    }
}
