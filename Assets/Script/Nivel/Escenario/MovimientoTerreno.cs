﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoTerreno : MonoBehaviour {
    public bool usarVelGlobal = true;

    /// <summary>
    /// Unidades que se trasladan por segundo. (usarVelGlobal debe ser falso). 
    /// </summary>
    public float velTerreno = -1;

    public float i = 0;

	void Update ()
    {
        velTerreno = Spawn_ControlGeneral.velCaidaGlobal;        
        transform.position += new Vector3(0, velTerreno * Time.deltaTime, 0);      
    }
}
