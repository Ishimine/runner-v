﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moneda : MonoBehaviour
{
	void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Player")
        {
            GameObject txt = Instantiate(Resources.Load("txtPopUp", typeof(GameObject)),transform.position,transform.rotation) as GameObject;
            txt.GetComponent<TextMesh>().text = "+100";
            other.GetComponent<PjPickUpManager>().SumarMoneda();
            Destroy(gameObject);    
        }
        else if(other.tag == "AreaEliminacion")
        {
            Destroy(gameObject);
        }
    }
}
