﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PiezaEscenario  {

    [HideInInspector]
    public string nombre;
    public GameObject pieza;
    /// <summary>
    /// Mayor el valor de la pieza mayor la probabilidad de que la pieza sea creada
    /// </summary>
    [Range(0f,100f)]
    public float valor;
    /// <summary>
    /// Probabilidad actual de que la pieza en cuestion sea creada
    /// </summary>
    [SerializeField]
    public int probabilidad;

     public void SetProbabilidad(float x)
    {
        probabilidad = (int)x;
    }


}
