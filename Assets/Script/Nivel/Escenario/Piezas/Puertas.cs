﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Puertas {

    public bool Izq;
    public bool Med;
    public bool Der;

    public Puertas(bool i, bool m, bool d)
    {
        Izq = i;
        Med = m;
        Der = d;
    }

    public Puertas(bool a)
    {
        Izq = a;
        Med = a;
        Der = a;
    }

    /// <summary>
    /// Compara las entradas y salidas dadas y 
    /// </summary>
    /// <param name="entradas">Entradas de la pieza actual</param> 
    /// <param name="salidas">Salidas de la pieza anterior</param>
    /// <param name="hayQueEspejar"> Retorna Verdadero, en caso de que las salidas y entradas sean compatibles al espejar la pieza siguiente</param>
    /// <param name="sePuedeEspejarAleatoriamente">retorna verdadero si el espejar la pieza no afecta la compatibildad de las piezas </param>
    /// <returns></returns>
    /// 


    public static bool EntradasSalidasCompatibles(Puertas entradas, Puertas salidas, ref bool hayQueEspejar)
    {
        if (salidas.Med == true && entradas.Med == true)
        {
            hayQueEspejar = false;
            return true;
        }
        else if (salidas.Izq == true)
        {
            if (entradas.Izq == true)
            {
                hayQueEspejar = false;
                return true;
            }
            else if (entradas.Der == true)
            {
                hayQueEspejar = true;
                return true;
            }
            else return false;
        }
        else if (salidas.Der == true)
        {
            if (entradas.Der == true)
            {
                hayQueEspejar = false;
                return true;
            }
            else if (entradas.Izq == true)
            {
                hayQueEspejar = true;
                return true;
            }
            else return false;
        }
        else
        {
            return false;
        }
    }

    public static bool EntradasSalidasCompatibles(Puertas entradas, Puertas salidas)
    {
        if ((salidas.Med == true && entradas.Med == true) || (salidas.Izq == true && entradas.Izq == true) || (salidas.Der == true && entradas.Der == true))
        {
            return true;
        }
        else
            return false;
    }

    public static bool EsCompatible(Puertas entradas, Puertas salidas)
    {
        if ((salidas.Med == true && entradas.Med == true) || 
            ((salidas.Izq == true || salidas.Der == true) && (entradas.Izq == true || entradas.Der == true)))
        {
            return true;
        }
        else
            return false;
    }

    public static bool HayQueEspejar(Puertas entradas, Puertas salidas)
    {
        if ((salidas.Med == true && entradas.Med == true) ||
             (salidas.Izq == true && entradas.Izq == true) || 
              (salidas.Der == true && entradas.Der == true))
        {
            return false;
        }
        else
            return true;
    }

    public static bool SePuedeEspejarAleatoriamente(Puertas entradas, Puertas salidas)
    {
        if ((salidas.Med == true && entradas.Med == true) ||
             ((salidas.Izq == true || salidas.Der == true) && (entradas.Izq == true && entradas.Der == true)) ||
              ((salidas.Izq == true && salidas.Der == true) && (entradas.Izq == true || entradas.Der == true)))
        {
            return true;
        }
        else
            return false;
    }

    public void Invertir()
    {
        bool aux = Izq;
        Izq = Der;
        Der = aux;
    }

    public void AbrirTodo()
    {
        Izq = true;
        Med = true;
        Der = true;
    }

    public void CopiarConfig(Puertas x)
    {
        Izq = x.Izq;
        Med = x.Med;
        Der = x.Der;
    }
}
