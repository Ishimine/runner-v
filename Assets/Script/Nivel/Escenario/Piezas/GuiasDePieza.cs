﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuiasDePieza : MonoBehaviour {

    public int largo = 256;
    [Header("Entradas")]
    public GameObject anclaEntradas;
    public GameObject entIzq;
    public GameObject entMed;
    public GameObject entDer;


    [Header("Salidas")]
    public GameObject anclaSalidas;
    public GameObject salIzq;
    public GameObject salMed;
    public GameObject salDer;
    

    public SpriteRenderer cuerpo;
    public GameObject ancla;
    public PiezaInfo pInfo;
    bool ok = true;
    public Color color;



    public void CheckeoGeneral()
    {
        cuerpo.color = color;
        ActualizarEntSal();
        ActualizarTamaño();

    }

    void ActualizarEntSal()
    {
        anclaSalidas.transform.localPosition = new Vector3(0, (float)pInfo.largo / 100);
        if (transform.parent.localScale.x == 1)
        {
            salIzq.SetActive(pInfo.salidas.Izq);
            salDer.SetActive(pInfo.salidas.Der);
            entIzq.SetActive(pInfo.entradas.Izq);
            entDer.SetActive(pInfo.entradas.Der);
        }
        else
        {
            salIzq.SetActive(pInfo.salidas.Der);
            salDer.SetActive(pInfo.salidas.Izq);
            entIzq.SetActive(pInfo.entradas.Der);
            entDer.SetActive(pInfo.entradas.Izq);
        }
        salMed.SetActive(pInfo.salidas.Med);
        entMed.SetActive(pInfo.entradas.Med);
    }

    void ActualizarTamaño()
    {
        ancla.transform.localScale = new Vector3(4.4f, (float)pInfo.largo / 100);
    }   


    void CheckParent()
    {
        if(transform.parent != null && transform.parent.GetComponent<PiezaInfo>() != null)
        {
            largo = transform.parent.GetComponent<PiezaInfo>().largo;
        }
    }

}
