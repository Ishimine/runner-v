﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PiezaInfo : MonoBehaviour {


    public TipoPiezaInfo TipoyDif;   

    public int largo = 256;
    [Header("Salidas")]
    [SerializeField]
    public Puertas salidas;
    [Header("Entradas")]
    [SerializeField]
    public Puertas entradas;
    public bool entradasEspejables = false;
    public bool hayQueEspejar = false;

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, transform.position + new Vector3(0, (float)largo / 100));
        Gizmos.DrawLine(transform.position - new Vector3(2.2f, - (float)largo / 100), transform.position + new Vector3(2.2f, (float)largo / 100));

    }



}
