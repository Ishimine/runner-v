﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

[RequireComponent(typeof(DirectorDeEscenario))]
public class GuardaCargarConfigDif : MonoBehaviour {

    public contenedorEsc contenedorEscGuardados;
    [HideInInspector]    public int cantidadDif;
    [HideInInspector]    public int slotObjetivo;
    [HideInInspector]    public int escenarioObjetivo;
    public DirectorDeEscenario dirEsc;
    
    void ActualizarCant()
    {
        cantidadDif = dirEsc.escenarios.Length;
    }

    public void GuardarEscenario()
    {
        Escenario nuevo = dirEsc.escenarios[escenarioObjetivo];
        contenedorEscGuardados.escenarios[slotObjetivo] = nuevo;
    }

    public void CargarEscenario()
    {
        //  dirEsc.escenarios[escenarioObjetivo] = contenedorEscGuardados.escenarios[slotObjetivo];
        Escenario nuevo = contenedorEscGuardados.escenarios[slotObjetivo];
        dirEsc.escenarios[escenarioObjetivo] = nuevo;

    }

}
