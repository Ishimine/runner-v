﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class Escenario {

    [Header("Parametros Generales")]
    public string nombreDelEscenario;
    public float Velocidad = -4;
    public int espacioEntrePiezas = 256;
    public int tamañoBloquesVacios = 256;
    public int duracion = 30;
    public PiezaEscenario[] piezas;
    public Color colorFondo;
}

