﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Spawner : MonoBehaviour {
    
    public bool FixedCuentaRegresiva(ref float cont)
    {
        cont -= Time.fixedDeltaTime;
        if (cont <= 0)
        {            
            return true;
        }
        else
        {
            return false;
        }
    }
    

    public void AceleracionDeVariable(ref float actual, float max, float suavizado, ref float vel)
    {
        actual = Mathf.SmoothDamp(actual, max, ref vel, suavizado * Time.fixedDeltaTime * 100);
    }

    /// <summary>
    /// Transicion del valor velActual, desde velInicial, hasta velMax, en el tiempo indicado por tTransicion.
    /// </summary>
    /// <param name="velActual"> Valor actual de la transicion.</param>
    /// <param name="velInicial">   Valor inicial</param>
    /// <param name="velMax">   Valor Final </param>
    /// <param name="tTransicion">  Tiempo de la transicion </param>  
    /// <param name="t">parametro auxiliar para usar en Mathf.Lerp</param>
    public void TransicionLinealEnT(ref float velActual, float velInicial, float velMax, float tTransicion, ref float t)
    {        
        t += Time.deltaTime / tTransicion;
        velActual = Mathf.Lerp(velInicial, velMax, t);
    }

    /// <summary>
    /// Devuelve la distancia recorrida de acuerdo a la velocidad global marcada por el Spawn_ControlGeneral
    /// </summary>
    /// <param name="uRecorridas"></param>
    public void GetDistanciaRecorrida(ref float uRecorridas)
    {
        float velocidad = Spawn_ControlGeneral.velCaidaGlobal;
        uRecorridas += Mathf.Abs(velocidad * Time.deltaTime * 100);
    }
}
