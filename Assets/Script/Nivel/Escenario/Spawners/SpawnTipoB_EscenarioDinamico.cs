﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnTipoB_EscenarioDinamico : Spawner {

    
    public Puertas salidasUltPieza = new Puertas(true);

    [SerializeField]
    private GameObject piezaSiguiente;

    enum TipoPieza { PickUp, PiezaFacil, PiezaMedio, PiezaDificil, Vacia, BloqueFacil, BloqueMedio, BloqueDificil};
    [SerializeField]    TipoPieza piezaAnt;
    [SerializeField]    TipoPieza piezaSig;

    [Header("Probabilidad por Pieza")]
    public float probPickUp;
    public float probFacil;
    public float probMedio;
    public float probDificil;
    public float probVacia;
    public float probBloqueFacil;
    public float probBloqueMedio;
    public float probBloqueDificil;


    [SerializeField]    private float pPickUp;
    [SerializeField]    private float pFacil;
    [SerializeField]    private float pMedio;
    [SerializeField]    private float pDificil;
    [SerializeField]    private float pVacia;
    [SerializeField]    private float pBloqueFacil;
    [SerializeField]    private float pBloqueMedio;
    [SerializeField]    private float pBloqueDificil;


    public GameObject[] piezaPickUps;
    public GameObject[] piezasFacil;
    public GameObject[] piezasMedio;
    public GameObject[] piezasDificil;
    public GameObject[] bloquesFacil;
    public GameObject[] bloquesMedio;
    public GameObject[] bloquesDificil;


    public int distCorta = 128;
    public int distMedia = 256;
    public int distLarga = 512;
    public float distUltBloque;

    public bool mostrarTimingDePieza = false;
    public GameObject txtPrefab;   
        
    [SerializeField]    private float distRecorrida;
    [SerializeField]    private float distObjetivo;

    private bool espejarAleatoriamente;
    private bool hayQueEspejar;
    void Start()
    {
        CalcularPorcentajesSpawn();

        GetPiezaSig(ref piezaSig);               //Consigue el TIPO de la siguiente pieza
        PrepararSigPieza(piezaSig);
    }

    void Update()
    {
        GetDistanciaRecorrida(ref distRecorrida);       //Actualiza Las unidades recorridas en cada Update             

        if (distRecorrida >= distObjetivo)
        {
            distRecorrida = 0;                       //Reinicia el contador de distancia
            piezaAnt = piezaSig;                     //Guarda la pieza anterior

            CrearSigPieza(piezaSiguiente);          //Crea la pieza preparada                        

            GetPiezaSig(ref piezaSig);               //Consigue el TIPO de la siguiente pieza
            PrepararSigPieza(piezaSig);             //Se prepara la "piezaSiguiente" pieza en base al tipo de "piezaSig"o.


            distObjetivo = GetTiempoSig(piezaSig,piezaAnt);      //Consigue el nuevo tiempo basado en la pieza anterior y en la siguiente          

            ModTiempoPorBloque(ref distObjetivo);               //Modificacion Especial en caso de ser un BLOQUE
            ModTiempoPorPickUp(ref distObjetivo);               //Modificacion Especial en caso de ser un PICKUP
        }
    }


    void ModTiempoPorBloque(ref float distObj)
    {
        if ((piezaSig == TipoPieza.BloqueFacil)|| (piezaSig == TipoPieza.BloqueMedio)|| (piezaSig == TipoPieza.BloqueDificil))         //En caso de ser un bloque se le agrega el tama;o del bloque en cuestion
        {
            distObj += piezaSiguiente.GetComponent<PiezaInfo>().largo;
            distUltBloque = distObj;
            //print("Piesa Sig Bloque: distObj = " + distObj);
        }
        else if ((piezaAnt == TipoPieza.BloqueFacil) || (piezaAnt == TipoPieza.BloqueMedio) || (piezaAnt == TipoPieza.BloqueDificil)) 
        {
            distObj += distUltBloque;
            //print("Piesa Ant Bloque: distObj = " + distObj);
        }
    }

    void ModTiempoPorPickUp(ref float distObj)
    {
        if  ((piezaAnt == TipoPieza.PickUp) && 
             (piezaSig == TipoPieza.BloqueFacil) ||
              (piezaSig == TipoPieza.BloqueMedio) ||
               (piezaSig == TipoPieza.BloqueDificil))
        {
            distObj = distObj / 1.5f;
        }
        else if ((piezaSig == TipoPieza.PickUp) &&
                 (piezaAnt == TipoPieza.BloqueFacil) ||
                  (piezaAnt == TipoPieza.BloqueMedio) ||
                   (piezaAnt == TipoPieza.BloqueDificil))
        {
            distObj = distObj / 1.5f;
        }
    }

    float GetTiempoSig(TipoPieza pSig,TipoPieza pAnt)
    {        
        if (pAnt == TipoPieza.PiezaDificil && pSig == TipoPieza.PiezaDificil) 
        {
            return distLarga;
        }
        else if(    (pAnt == TipoPieza.PiezaDificil && pSig == TipoPieza.PiezaMedio ) 
                ||  (pAnt == TipoPieza.PiezaMedio   && pSig == TipoPieza.PiezaMedio)
                ||  (pAnt == TipoPieza.PiezaMedio    && pSig == TipoPieza.PiezaDificil))
            return distMedia;
        else
        {
            return distCorta;
        }
    }

    void GetPiezaSig (ref TipoPieza pSig)
    {
        int i = Random.Range(1,100);
        if (i <= pPickUp)
        {
            pSig = TipoPieza.PickUp;
        }
        else if (i <= pFacil + pPickUp)
        {
            //print("PiezaFacil");
            pSig = TipoPieza.PiezaFacil;
        }
        else if (i <= pMedio + pFacil + pPickUp)
        {
            //print("PiezaMedio");
            pSig = TipoPieza.PiezaMedio;
        }
        else if(i <= pDificil + pMedio + pFacil + pPickUp)
        {
            //print("PiezaDificil");
            pSig = TipoPieza.PiezaDificil;
        }
        else if(i <= pVacia + pDificil + pMedio + pFacil + pPickUp)
        {
            //print("BloqueVacia");
            pSig = TipoPieza.Vacia;
        }
        else if (i <= pBloqueFacil + pVacia + pDificil + pMedio + pFacil + pPickUp)            
        {
            print("BloqueFacil");
            pSig = TipoPieza.BloqueFacil;
        }
        else if (i <= pBloqueMedio + pBloqueFacil + pVacia + pDificil + pMedio + pFacil + pPickUp)
        {
            print("BloqueMedio");
            pSig = TipoPieza.BloqueMedio;
        }
        else
        {            
            print("BloqueDificil");
            pSig = TipoPieza.BloqueDificil;
        }
    }

    void CalcularPorcentajesSpawn()
    {
        float total = probPickUp + probFacil + probMedio + probDificil + probVacia + probBloqueFacil + probBloqueMedio + probBloqueDificil;
        pPickUp = 100 / (total / probPickUp);
        pFacil = 100 / (total / probFacil);
        pMedio = 100 / (total / probMedio);
        pDificil = 100 / (total / probDificil);
        pVacia = 100 / (total / probVacia);
        pBloqueFacil = 100 / (total / probBloqueFacil);
        pBloqueMedio = 100 / (total / probBloqueMedio);
        pBloqueDificil = 100 / (total / probBloqueDificil);
    }

    void PrepararSigPieza(TipoPieza pSig)
    {
        switch (pSig)
        {
            case TipoPieza.PickUp:
                CargarSigPieza(piezaPickUps);
                break;
            case TipoPieza.PiezaFacil:
                CargarSigPieza(piezasFacil);
                break;
            case TipoPieza.PiezaMedio:
                CargarSigPieza(piezasMedio);
                break;
            case TipoPieza.PiezaDificil:
                CargarSigPieza(piezasDificil);
                break;
            case TipoPieza.BloqueFacil:
                CargarSigPieza(bloquesFacil);
                break;
            case TipoPieza.BloqueMedio:
                CargarSigPieza(bloquesMedio);
                break;
            case TipoPieza.BloqueDificil:
                CargarSigPieza(bloquesDificil);
                break;
            case TipoPieza.Vacia:
                piezaSiguiente = null;
                break;
            default:
                piezaSiguiente = null;
                break;
        }
    }


    void CargarSigPieza(GameObject[] piezas)
    {
        bool ok = false;

        while (!ok)
        {
            print("Piezas: " + piezas.Length);
            int i = Random.Range(0, piezas.Length);
            print("Numero: " + i);

            if(Puertas.EntradasSalidasCompatibles(piezas[i].GetComponent<PiezaInfo>().entradas,salidasUltPieza,ref hayQueEspejar))
            {
                salidasUltPieza = piezaSiguiente.GetComponent<PiezaInfo>().salidas;
                piezaSiguiente = piezas[i];               
                if(hayQueEspejar)
                {
                    print(salidasUltPieza);
                    //   salidasUltPieza.Invertir();
                    bool x = salidasUltPieza.Der;
                    salidasUltPieza.Der = salidasUltPieza.Izq;
                    salidasUltPieza.Izq = x;
                    print(salidasUltPieza);
                }
                ok = true;
            }
        }
    }


 
    void CrearSigPieza(GameObject sigPieza)
    {
        #region Escepcion para piezas VACIA/Descanso
        if (piezaSiguiente == null)
        {
            salidasUltPieza = new Puertas(true);
            distUltBloque = 0;
            return;
        }
        #endregion

        #region EspejarPiezaAleatoriamente
        int i = 1;
        if (espejarAleatoriamente)
        {
            i = Random.Range(1, 3);
            if (i != 1) i = -1;
        }
        else if (hayQueEspejar) i = -1;
        #endregion




        GameObject clone = (GameObject)Instantiate(sigPieza, transform.position, sigPieza.transform.rotation);
        clone.GetComponent<MovimientoTerreno>().velTerreno = Spawn_ControlGeneral.velCaidaGlobal;
        clone.transform.localScale = new Vector3(i,1,1);

        if (mostrarTimingDePieza)
        {
            GameObject texto = (GameObject)Instantiate(txtPrefab, transform);
            texto.GetComponent<TextMesh>().text = distObjetivo.ToString("0.00");
            texto.transform.SetParent(clone.transform);
            Destroy(texto.gameObject, 6f);
        }
        Destroy(clone.gameObject, 6f);
    }

    void CrearPiezaAleatoria(GameObject[] piezas)
    {
        int i = Random.Range(0, piezas.Length);

        GameObject clone = (GameObject)Instantiate(piezas[i], transform.position, piezas[i].transform.rotation);
        clone.GetComponent<MovimientoTerreno>().velTerreno = Spawn_ControlGeneral.velCaidaGlobal;

        if (mostrarTimingDePieza)
        {
            GameObject texto = (GameObject)Instantiate(txtPrefab, transform);
            texto.GetComponent<TextMesh>().text = distObjetivo.ToString("0.00");
            texto.transform.SetParent(clone.transform);
            Destroy(texto.gameObject, 6f);
        }
        Destroy(clone.gameObject, 6f);
    }
}
