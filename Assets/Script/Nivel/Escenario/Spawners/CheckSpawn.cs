﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckSpawn : MonoBehaviour {

    [SerializeField]
    private bool canSpawn;

    public bool CanSpawn
    {
        get
        {
            return canSpawn;
        }
        set
        {
            canSpawn = value;
        }
    }

/*	void OnTriggerStay2D(Collider2D other)
    {
        print("Stay");
        canSpawn = false;
    }
    */

    void OnTriggerExit2D(Collider2D other)
    {
        canSpawn = true;
    }

}
