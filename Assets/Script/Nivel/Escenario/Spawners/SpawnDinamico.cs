﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnDinamico : Spawner
{ 
      
    public int espacioEntrePiezas = 256;
    public int espaciosVacios = 128;

    public TipoPiezaInfo infoPiezaAnt = new TipoPiezaInfo();
    public TipoPiezaInfo infoPiezaSig = new TipoPiezaInfo();

    public Puertas salidaPiezaAnt = new Puertas(true);
    public Puertas entradaPiezaAnt = new Puertas(true);
    
    [Header("Probabilidades por Tipo")]
    public float probPickUp;
    public float probSueltas;
    public float probBloques;
    public float probVacio;

    [Header("Probabilidades por Dificultad")]
    public float probFacil;
    public float probMedio;
    public float probDificil;

    [Header("ProbFinal por Tipo")]
    [SerializeField]
    private float pFinalPickUp;
    [SerializeField]
    private float pFinalSueltas;
    [SerializeField]
    private float pFinalBloques;
    [SerializeField]
    private float pFinalVacio;



    [Header("ProbFinal por Dificultad")]
    [SerializeField]
    private float pFinalFacil = 0;
    [SerializeField]
    private float pFinalMedio = 0;
    [SerializeField]
    private float pFinalDificil = 0;

    List<GameObject> piezasValidas = new List<GameObject>();

    public GameObject[] pPickUp;
    public GameObject[] pSueltas;
    public GameObject[] pBloques;
    private GameObject piezaSiguiente;

    public float distRecorrida;
    public float distObjetivo;

    bool hayQueEspejar;
    bool sePuedeEspejarAleatoriamente;

    public bool mostrarTimingDePieza = false;
    public GameObject txtPrefab;


    void Start()
    {
        CalcularPorcentajesPorTipo();
        CalcularPorcentajesPorDificultad();
        GetTipoSig();                           //Consigue el tipo siguiente y almacena el previo
        GetDificultadSig();                     //Consigue la Dificultad Siguiente y almacena la Previa
        PrepararSigPieza();
    }


    public void CalcularPorcentajesPorTipo()
    {
        float total = probPickUp + probSueltas + probBloques + probVacio;
        pFinalPickUp = 100 / (total / probPickUp);
        pFinalSueltas = 100 / (total / probSueltas);
        pFinalBloques = 100 / (total / probBloques);
        pFinalVacio = 100 / (total / probVacio);
    }

    public void CalcularPorcentajesPorDificultad()
    {
        float total = probFacil + probMedio + probDificil;
        pFinalFacil = 100 / (total / probFacil);
        pFinalMedio = 100 / (total / probMedio);
        pFinalDificil = 100 / (total / probDificil);
    }

    void Update()
    {
        GetDistanciaRecorrida(ref distRecorrida);           //Actualiza Las unidades recorridas en cada Update             

        if (distRecorrida >= distObjetivo)
        {
            // print("asd");
            distRecorrida = 0;                              //Reinicia el contador de distancia 

            CrearSigPieza();                                //Crea la pieza preparada

            GetTipoSig();                                   //Consigue el tipo siguiente y almacena el previo
            if (infoPiezaSig.tipo == TipoPiezaInfo.TipoPieza.PickUp)
            {
                infoPiezaSig.dificultad = TipoPiezaInfo.DifPieza.Facil;
            }
            else
            {
                GetDificultadSig();                         //Consigue la Dificultad Siguiente y almacena la Previa
            }
            PrepararSigPieza();                             //Se prepara la "piezaSiguiente" pieza en base al tipo de "piezaSig"o.

        }
    }

    void GetTipoSig()
    {
        infoPiezaAnt.tipo = infoPiezaSig.tipo;
        int i = Random.Range(1, 100);

        if (i <= pFinalPickUp)
            infoPiezaSig.tipo = TipoPiezaInfo.TipoPieza.PickUp;
        else if (i <= pFinalSueltas + pFinalPickUp)
            infoPiezaSig.tipo = TipoPiezaInfo.TipoPieza.Suelta;
        else if (i <= pFinalBloques + pFinalSueltas + pFinalPickUp)
            infoPiezaSig.tipo = TipoPiezaInfo.TipoPieza.Bloque;
        else
            infoPiezaSig.tipo = TipoPiezaInfo.TipoPieza.Vacia;
    }

    void GetDificultadSig()
    {
        infoPiezaAnt.dificultad = infoPiezaSig.dificultad;
        int i = Random.Range(1, 100);
        if (i <= pFinalFacil)
            infoPiezaSig.dificultad = TipoPiezaInfo.DifPieza.Facil;
        else if (i <= pFinalMedio + pFinalFacil)
            infoPiezaSig.dificultad = TipoPiezaInfo.DifPieza.Medio;
        else infoPiezaSig.dificultad = TipoPiezaInfo.DifPieza.Dificil;
    }

    void PrepararSigPieza()
    {
        switch (infoPiezaSig.tipo)
        {
            case TipoPiezaInfo.TipoPieza.PickUp:
                SeleccionarPieza(pPickUp);
                //print("1");
                break;
            case TipoPiezaInfo.TipoPieza.Suelta:
                SeleccionarPieza(pSueltas);
                //print("2");
                break;
            case TipoPiezaInfo.TipoPieza.Bloque:
                SeleccionarPieza(pBloques);
                //print("3");
                break;
            case TipoPiezaInfo.TipoPieza.Vacia:
                piezaSiguiente = null;
                // print("4");
                break;
            default:
                //print("5");
                break;
        }
    }

    void SeleccionarPieza(GameObject[] piezas)
    {
        //print("qqqq");
        piezasValidas.Clear();

        for (int i = 0; i < piezas.Length; i++)
        {
            if ((piezas[i].GetComponent<PiezaInfo>().TipoyDif.dificultad == infoPiezaSig.dificultad) &&
                (Puertas.EsCompatible(piezas[i].GetComponent<PiezaInfo>().entradas, salidaPiezaAnt)))
            {
                //("sss");
                piezasValidas.Add(piezas[i]);
            }
        }

        if (piezasValidas.Count == 0)
        {
            piezaSiguiente = null;
        }
        else
        {
            int i = Random.Range(0, piezasValidas.Count);
            piezaSiguiente = piezasValidas[i];

        }
    }

    void CrearSigPieza()
    {
        #region IF pieza NULL
        if (piezaSiguiente == null)
        {
            salidaPiezaAnt.AbrirTodo();
            distObjetivo = espaciosVacios;
            return;
        }
        #endregion


        //HayQueInvertir? SePuedeInvertirAleatoriamente?
        int i = 1;
        if (Puertas.HayQueEspejar(piezaSiguiente.GetComponent<PiezaInfo>().entradas, salidaPiezaAnt))
        {
            //print("Hay que espejar");
            i = -1;
        }
        else if (Puertas.SePuedeEspejarAleatoriamente(piezaSiguiente.GetComponent<PiezaInfo>().entradas, salidaPiezaAnt))
        {
            //print("Se puede espejar");
            i = Random.Range(1, 3);
            if (i != 1)
            {
                i = -1;
            }
        }







        //MODIFICAR ESTO MAS TARDE PARA USAR UN POOL
        GameObject clone = (GameObject)Instantiate(piezaSiguiente, transform.position, piezaSiguiente.transform.rotation);
        clone.GetComponent<MovimientoTerreno>().velTerreno = Spawn_ControlGeneral.velCaidaGlobal;
        clone.transform.localScale = new Vector3(i, 1, 1);

        if (mostrarTimingDePieza)
        {
            GameObject texto = (GameObject)Instantiate(txtPrefab, transform);
            texto.GetComponent<TextMesh>().text = distObjetivo.ToString("0.00");
            texto.transform.SetParent(clone.transform);
            Destroy(texto.gameObject, 6f);
        }
        Destroy(clone.gameObject, 6f);
        distObjetivo = piezaSiguiente.GetComponent<PiezaInfo>().largo + espacioEntrePiezas;




        //Se actualizan las salidas de la ultima pieza
        entradaPiezaAnt.CopiarConfig(piezaSiguiente.GetComponent<PiezaInfo>().entradas);
        salidaPiezaAnt.CopiarConfig(piezaSiguiente.GetComponent<PiezaInfo>().salidas);
        if (i == -1)
        {
            salidaPiezaAnt.Invertir();
        }

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="a"></param>
    /// Prob. para PickUp
    /// <param name="b"></param>
    /// Prob. para Piezas Sueltas
    /// <param name="c"></param>
    ///  Prob. para Bloques
    /// <param name="d"></param>
    ///  Prob. para espacios Vacios
   public void ReemplazarProbPorTipo(float a, float b, float c, float d)
    {
        probPickUp = a;
        probSueltas = b;
        probBloques = c;
        probVacio = d;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="a"></param>
    ///  Prob. para Piezas/Bloques de grado Facil
    /// <param name="b"></param>
    /// Prob. para Piezas/Bloques de grado Medio
    /// <param name="c"></param>
    /// Prob. para Piezas/Bloques de grado Dificil
    public void ReemplazarProbPorDificultad(float a, float b, float c)
    {
        probFacil = a;
        probMedio = b;
        probDificil = c;
    }

    public void ReemplazarEspacios(int a, int b)
    {
        espacioEntrePiezas = a;
        espaciosVacios = b;
    }
}
