﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn_ControlGeneral : Spawner {

    public static float velCaidaGlobal;

    public float velInicial = -4;
    public float velMax = -10;
    public float velActual = 0;
    public float tiempoTransicion;
    private float t;

    float vel;
    bool listo = false;
    public bool gradual = true;



    void Update()
    {
        velCaidaGlobal = velActual;
        if (gradual)
        {
            AumentoGradual();
        }
    }

    void AumentoGradual()
    {
        TransicionLinealEnT(ref velActual, velInicial, velMax, tiempoTransicion, ref t);       
        if (velActual == velMax && !listo)
        {
            Debug.Log("Se termino la transicion en: " + Time.time);
            listo = true;
        }
    }
}
