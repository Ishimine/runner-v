﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEscenarios : Spawner
{
    [Header("Distancias")]
    [SerializeField]
    private float distRecorrida = 0;
    private float distTotal = 0;
    [SerializeField]
    private float distObjetivo = 0;
    public float espacioEntrePiezas;
    public float tamañoBloqueVacio;
    public bool mostrarGuiaDePieza;
    public GameObject guiaPrefab;


    public PiezaEscenario[] piezas;

    [Header("Info Piezas Ant/Sig")]
    public GameObject piezaSiguiente;
    public Puertas entradaPiezaSig = new Puertas(true);
    public Puertas salidaPiezaAnt = new Puertas(true);
    public Puertas entradaPiezaAnt = new Puertas(true);


    [HideInInspector]
    public bool go = false;

    public void CalcularPorcentajes()
    {
        //Calculamos el total
        float total = 0;
        for (int a = 0; a < piezas.Length; a++)
        {
            total += piezas[a].valor;
        }
        //Repartimos las probabilidades en base del total
        for (int a = 0; a < piezas.Length; a++)
        {
            piezas[a].SetProbabilidad((100 / (total / piezas[a].valor)));
        }
    }

   
    void Update()
    {
        if(!go)
        {
            return;
        }
        GetDistanciaRecorrida(ref distRecorrida);
        GetDistanciaRecorrida(ref distTotal);

        if (distRecorrida >= distObjetivo)
        {
            distRecorrida = 0;
            CrearSigPieza();

            BuscarPiezaSiguiente();
        }
    }

    void CrearSigPieza()
    {
        #region IF pieza NULL se deja un bloque vacio
        if (piezaSiguiente == null)
        {
            salidaPiezaAnt.AbrirTodo();
            distObjetivo = tamañoBloqueVacio;
            return;
        }
        #endregion
        //HayQueInvertir? SePuedeInvertirAleatoriamente?
        int i = 1;
        if (Puertas.HayQueEspejar(entradaPiezaSig, salidaPiezaAnt))
        {
            i = -1;
        }
        else if (Puertas.SePuedeEspejarAleatoriamente(entradaPiezaSig, salidaPiezaAnt))
        {
            i = Random.Range(1, 3);
            if (i != 1)
            {
                i = -1;
            }
        }

        GameObject clone = (GameObject)Instantiate(piezaSiguiente, transform.position, piezaSiguiente.transform.rotation);
        clone.GetComponent<MovimientoTerreno>().velTerreno = Spawn_ControlGeneral.velCaidaGlobal;
        clone.transform.localScale = new Vector3(i, 1, 1);
        Destroy(clone.gameObject, 6f);
        distObjetivo = piezaSiguiente.GetComponent<PiezaInfo>().largo + espacioEntrePiezas;

       

        //Se actualizan las salidas de la ultima pieza
        salidaPiezaAnt.CopiarConfig(piezaSiguiente.GetComponent<PiezaInfo>().salidas);
        entradaPiezaAnt.CopiarConfig(piezaSiguiente.GetComponent<PiezaInfo>().entradas);
        if (i == -1)
        {
            salidaPiezaAnt.Invertir();
            entradaPiezaAnt.Invertir();
        }




        if (mostrarGuiaDePieza)
        {
            GameObject guia = (GameObject)Instantiate(guiaPrefab, clone.transform);
            guia.transform.localPosition = Vector3.zero;
            guia.GetComponent<GuiasDePieza>().pInfo = clone.GetComponent<PiezaInfo>();
            guia.GetComponent<GuiasDePieza>().CheckeoGeneral();
        }
    }


 

    public void BuscarPiezaSiguiente()
    {

        int i = Random.Range(0, piezas.Length);

        if (Puertas.EsCompatible(piezas[i].pieza.GetComponent<PiezaInfo>().entradas, salidaPiezaAnt))
        {
            piezaSiguiente = piezas[i].pieza;
            entradaPiezaSig = piezaSiguiente.GetComponent<PiezaInfo>().entradas;
        }
        else
        {
            //BuscarPiezaSiguiente();
            piezaSiguiente = null;
        }
    }

    void OnValidate()
    {
        CalcularPorcentajes();
    }

    public void CargarNuevoEscenario(Escenario n)
    {
        tamañoBloqueVacio = n.tamañoBloquesVacios;
        espacioEntrePiezas =n.espacioEntrePiezas;
        piezas = n.piezas;
    }
}
        
    