﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnTipoB_Paredes : Spawner {

    [Header("Spawners")]
    public CheckSpawn spawnerIzq;
    public CheckSpawn spawnerDer;

    public GameObject[] pfParedes;

    [SerializeField]
    private float unidadesRecorridas;
    public int unidadesParaSpawn;
    float vel;

    void Update()
    {
        GetDistanciaRecorrida(ref unidadesRecorridas);
        if (unidadesRecorridas >= unidadesParaSpawn)
        {
            unidadesRecorridas = 0;
            SpawnParedes(spawnerIzq);
            SpawnParedes(spawnerDer);
        }
    }

    void SpawnParedes(CheckSpawn spawnPoint)
    {
        if (spawnPoint.CanSpawn)
        {
            int i = Random.Range(0, pfParedes.Length);
            GameObject clone = (GameObject)Instantiate(pfParedes[i], spawnPoint.transform.position, spawnPoint.transform.rotation);
        }
    }

   

}
