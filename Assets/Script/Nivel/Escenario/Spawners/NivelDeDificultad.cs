﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class NivelDeDificultad
{
    [Header("Parametros Generales")]
    public string nombreDelNivel;
    public float Velocidad = -4;
    public int espacioEntrePiezas = 256;
    public int espacioVacios = 256;


    /// <summary>
    /// Mientras mas alto sea el valor de un Tipo/Dificultad, mas probabilidad existira de que se genere una pieza de dicho Tipo/Dificultad
    /// </summary>
    [Header("Probabilidades por Tipo")]
    public float pickUp;
    public float sueltas;
    public float bloques;
    public float vacio;
    /// <summary>
    /// Mientras mas alto sea el valor de un Tipo/Dificultad, mas probabilidad existira de que se genere una pieza de dicho Tipo/Dificultad
    /// </summary>
    [Header("Probabilidades por Dificultad")]
    public float facil;
    public float medio;
    public float dificil;

}
