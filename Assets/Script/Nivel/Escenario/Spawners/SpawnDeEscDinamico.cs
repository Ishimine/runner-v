﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnDeEscDinamico : Spawner {


    enum TipoPieza {PickUp, PiezaFacil, PiezaMedio, PiezaDificil};
    TipoPieza piezaAnt;

    /// <summary>
    /// valores mas altos dan mas probabilidad de spawnear Obstaculos y viceversa para piezas 
    /// 2 = %50 | 3 = 66% obstaculos | 4 = 75% obstaculos
    /// </summary>
    public int probabilidadDeSpawn = 2;

    public Spawn_ControlGeneral spawnCG;

    public GameObject[] piezaObstFacil;
    public GameObject[] piezaObstMedio;
    public GameObject[] piezaObstDif;


    public GameObject[] piezaPickUps;

    [Header("Param. Spawn")]
    private float contador;

    public float tSpawnObstaMin = 0.3f;
    public float tSpawnObstaMax = 1f;

    public float tSpawnPickUpMin = 0.2f;
    public float tSpawnPickUpMax = 5f;


    public int nextPiezaTipo;

    public bool mostrarTimingDePieza = false;
    public GameObject txtPrefab;


    private float ultPiezaT;

    void FixedUpdate()
    {
        if (FixedCuentaRegresiva(ref contador)) //se encarga de dar la cuenta regresiva hasta 0 y luego crea un nuevo valor para la cuenta regresiva aleatoriamente entre 
        {
            if (nextPiezaTipo == 1)
            {
                int i = Random.Range(0, piezaPickUps.Length);
                CrearPieza(piezaPickUps[i]);
                piezaAnt = TipoPieza.PickUp;
            }
            else
            {
                int i = Random.Range(0, piezaObstFacil.Length + piezaObstMedio.Length);
                if(i < piezaObstFacil.Length)
                {
                    CrearPieza(piezaObstFacil[i]);
                    piezaAnt = TipoPieza.PiezaFacil;
                }
                else
                {
                    i -= piezaObstFacil.Length;
                    CrearPieza(piezaObstMedio[i]);
                    piezaAnt = TipoPieza.PiezaMedio;
                }                
            }

            nextPiezaTipo = Random.Range(1, probabilidadDeSpawn);

            if (nextPiezaTipo == 1)
            {
                contador = Random.Range(tSpawnPickUpMin, tSpawnPickUpMax);
                ultPiezaT = contador;
            }
            else
            {
                contador = Random.Range(tSpawnObstaMin, tSpawnObstaMax);
                ultPiezaT = contador;
            }
            if(piezaAnt == TipoPieza.PiezaMedio)
            {
                contador *= 2;
            }

        }
    }


    void CrearPieza(GameObject pieza)
    {        
        GameObject clone = (GameObject)Instantiate(pieza, transform.position, pieza.transform.rotation);
        clone.GetComponent<MovimientoTerreno>().velTerreno = spawnCG.velActual;
        if(mostrarTimingDePieza)
        {
            GameObject texto = (GameObject)Instantiate(txtPrefab, transform);
            texto.GetComponent<TextMesh>().text = ultPiezaT.ToString("0.00");
            texto.transform.SetParent(clone.transform);
       //     texto.transform.position = Vector3.zero;

        }
    }
}
