﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TipoPiezaInfo {

    public enum TipoPieza { PickUp, Suelta, Bloque, Vacia };    
    public TipoPieza tipo;

    public enum DifPieza { Facil, Medio, Dificil }
    public DifPieza dificultad;


    public TipoPiezaInfo()
    {
        tipo = TipoPieza.PickUp;
        dificultad = DifPieza.Facil;
    }
}
