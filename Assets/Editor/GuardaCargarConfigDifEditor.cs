﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


[CustomEditor(typeof(GuardaCargarConfigDif))]
public class GuardaCargarConfigDifEditor : Editor
{


    string[] opcionesEscenario = new string[10];
    int selecEscenario = 0;

    string[] opcionesSlot = new string[10];
    int selecSlot = 0;


    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        GuardaCargarConfigDif miScript = (GuardaCargarConfigDif)target;

 


        ActualizarNombreOpcEsc(miScript);
        ActualizarNombreSelSlot(miScript);
        
     
        
        EditorGUILayout.LabelField("EnUso", EditorStyles.boldLabel);
        selecEscenario = EditorGUILayout.Popup(selecEscenario, opcionesEscenario);

        EditorGUILayout.LabelField("Guardados", EditorStyles.boldLabel);
        selecSlot = EditorGUILayout.Popup(selecSlot, opcionesSlot);

        miScript.slotObjetivo = selecSlot;
        miScript.escenarioObjetivo = selecEscenario;





       // miScript.slotObjetivo = EditorGUILayout.IntField("Experience", myTarget.experience);

        if (GUILayout.Button("Guardar Escenario"))
        {
            miScript.GuardarEscenario();
        }

        if (GUILayout.Button("Cargar Escenario"))
        {
            miScript.CargarEscenario();
        }
        base.OnInspectorGUI();
    }


    void ActualizarNombreOpcEsc(GuardaCargarConfigDif miS)
    {
       opcionesEscenario = new string[miS.dirEsc.escenarios.Length];
       for (int i = 0; i < opcionesEscenario.Length; i++)
        {
            opcionesEscenario[i] = i + ": " + miS.dirEsc.escenarios[i].nombreDelEscenario;
        }
    }

    void ActualizarNombreSelSlot(GuardaCargarConfigDif miS)
    {
        opcionesSlot = new string[miS.contenedorEscGuardados.escenarios.Length];


        for (int i = 0; i < opcionesSlot.Length; i++)
        {
            if (miS.contenedorEscGuardados.escenarios[i] != null) opcionesSlot[i] = i + " : " + miS.contenedorEscGuardados.escenarios[i].nombreDelEscenario;
            else opcionesSlot[i] = i + ": Vacio ";
        }
    }
}
