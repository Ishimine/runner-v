﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VistaPreviaPieza : MonoBehaviour {

    public GameObject vistaPrevia;
    public Image imagenActual;
    public Transform puntoAncla;
    public GameObject guiaVP;
    GameObject objActual;
    BotonPieza selActual;


	void Start () {
		
	}

    public void PiezaSeleccionada(BotonPieza x)
    {
        selActual = x;
    }

    public void CambiarImagen ()
    {
        Destroy(objActual);
        objActual = selActual.pieza;
        objActual = Instantiate<GameObject>(objActual, puntoAncla.transform) as GameObject;
        objActual.transform.localPosition = Vector2.zero;
        objActual.transform.localScale = new Vector3(28,28,28);
        guiaVP.transform.parent = objActual.transform;
    }

}
