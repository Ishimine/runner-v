﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ConfigDeEscenario
{
    public static ConfigDeEscenario actual;
    public bool mostrarTamañoPiezas = false;
    public bool mostrarNombreDeEsc = false;
    public bool usarDuracionGlobal = false;
    public int duracionGlobal = 60;
    public Escenario[] escenarios;   

}
