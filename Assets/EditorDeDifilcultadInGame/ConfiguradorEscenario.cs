﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfiguradorEscenario : MonoBehaviour {

    public ConfigDeEscenario configuracion;

	void Start () {
		
	}
	
	void Update ()
    {
		
	}

    public void GuardarConfig(int i)
    {
        ConfigDeEscenario.actual = configuracion;
        SaveLoad.Save(i);
    }

    public void CargarConfig(int i)
    {        
        SaveLoad.Load();
        configuracion = SaveLoad.savedConfig[i];
    }
}
