﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BotonPieza : MonoBehaviour {

    public Text txt;
    public GameObject pieza;
    public VistaPreviaPieza vistaPrevia;
    public AdministradorPiezas adminPiezas;

    void Start()
    {
        txt.text = pieza.name;
        vistaPrevia = FindObjectOfType<VistaPreviaPieza>();
        adminPiezas = FindObjectOfType<AdministradorPiezas>();
    }

	public void PiezaSeleccionada()
    {
        adminPiezas.PiezaSeleccionada(pieza);
        vistaPrevia.PiezaSeleccionada(this);
        vistaPrevia.CambiarImagen();
    }
}
