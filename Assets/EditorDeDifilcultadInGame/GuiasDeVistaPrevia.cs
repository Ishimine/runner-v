﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GuiasDeVistaPrevia : MonoBehaviour {

    [Header("Entradas")]
    public GameObject anclaEntradas;
    public GameObject entIzq;
    public GameObject entMed;
    public GameObject entDer;

    [Header("Salidas")]
    public GameObject anclaSalidas;
    public GameObject salIzq;
    public GameObject salMed;
    public GameObject salDer;

    public Slider slider;

    public Text limiteMedida;

    public SpriteRenderer cuerpo;
    public GameObject ancla;
    public PiezaInfo pInfo;
    bool ok = true;
    public Color color;

    public void CheckeoGeneral()
    {
        ActualizarTamaño();
        cuerpo.color = color;
        ActualizarEntSal();
    }

    void ActualizarEntSal()
    {       
        if (transform.parent.localScale.x == 1)
        {
            salIzq.SetActive(pInfo.salidas.Izq);
            salDer.SetActive(pInfo.salidas.Der);
            entIzq.SetActive(pInfo.entradas.Izq);
            entDer.SetActive(pInfo.entradas.Der);
        }
        else
        {
            salIzq.SetActive(pInfo.salidas.Der);
            salDer.SetActive(pInfo.salidas.Izq);
            entIzq.SetActive(pInfo.entradas.Der);
            entDer.SetActive(pInfo.entradas.Izq);
        }
        salMed.SetActive(pInfo.salidas.Med);
        entMed.SetActive(pInfo.entradas.Med);
    }

    void ActualizarTamaño()
    {
        ancla.transform.localScale = new Vector3(4.4f + 100, (float)pInfo.largo / 10f);
    }

    public void ActualizarMedidaLim()
    {
        if (pInfo != null)
        {
            transform.parent.SetParent(null);
            limiteMedida.text = slider.value.ToString();
            pInfo.largo = (int)slider.value;
            ActualizarTamaño();
            transform.parent.SetParent(pInfo.gameObject.transform);
        }
    }

}
