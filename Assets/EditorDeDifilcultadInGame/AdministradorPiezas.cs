﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AdministradorPiezas : MonoBehaviour {

    public Transform contenedorActivas;
    public Transform contenedorInactivas;

    public List<GameObject> piezasActivas = new List<GameObject>();
    public List<GameObject> piezasInactivas = new List<GameObject>();

    [SerializeField]
    public GameObject botonBase;
    public GameObject piezaSeleccionada;
    public GuiasDeVistaPrevia guia;

    public void HacerBotones(List<GameObject> piezas, Transform contenedor)
    {   
        foreach(GameObject x in piezas)
        {

        }
    }

    void HacerBoton(Transform padre)
    {
        GameObject nuevoBoton = Instantiate<GameObject>(botonBase, padre); 
    }	

    public void Agregar()
    {
        if(piezaSeleccionada.transform.parent == contenedorActivas)
        {
            return;
        }

        piezaSeleccionada.transform.SetParent(contenedorActivas);

        piezasActivas.Add(piezaSeleccionada);
        piezasInactivas.Remove(piezaSeleccionada);

    }

    public void Quitar()
    {
        if (piezaSeleccionada.transform.parent == contenedorInactivas)
        {
            return;
        }
        piezaSeleccionada.transform.SetParent(contenedorInactivas);

        piezasInactivas.Add(piezaSeleccionada);
        piezasActivas.Remove(piezaSeleccionada);
    }

    public void AgregarPieza (GameObject x, List<GameObject> listaPiezas)
    {
        
	}

    public void PiezaSeleccionada(GameObject pieza)
    {
        piezaSeleccionada = pieza;
        guia.pInfo = pieza.GetComponent<PiezaInfo>();
    }
}
