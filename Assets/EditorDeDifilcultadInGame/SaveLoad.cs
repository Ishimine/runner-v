﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public static class SaveLoad  {

    public static ConfigDeEscenario[] savedConfig = new ConfigDeEscenario[3];

    public static void Save(int i)
    {
        savedConfig[i] = ConfigDeEscenario.actual;
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/savedConfig.txt");
        bf.Serialize(file, SaveLoad.savedConfig);
        file.Close();
    }

    public static void Load()
    {
        if (File.Exists(Application.persistentDataPath + "/savedConfig.txt"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/savedConfig.txt", FileMode.Open);
            SaveLoad.savedConfig = (ConfigDeEscenario[])bf.Deserialize(file);
            file.Close();
        }
    }
}
